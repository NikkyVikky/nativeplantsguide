package com.nativeplantguide.app.aboutus;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.nativeplantguide.app.R;
import com.nativeplantguide.app.customViews.NativePlantTextView;
/**
 * This Activity class is used to display the about us list
 *
 * @author Innoppl Technologies.
 * @version 1.0
 */
public class AboutUsMenuActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us_menu);
        initViews();
    }

    private void initViews() {
        NativePlantTextView aboutUsMenu1=findViewById(R.id.aboutUsMenu1);
        NativePlantTextView aboutUsMenu2=findViewById(R.id.aboutUsMenu2);
        ImageView backIconIv=findViewById(R.id.back_icon);

        aboutUsMenu1.setOnClickListener(this);
        aboutUsMenu2.setOnClickListener(this);
        backIconIv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()){

            case R.id.aboutUsMenu1:

                callIntent("Al Ain City Municipality");
                break;
            //AlerDialog when click on Exit
            case R.id.aboutUsMenu2:
             callIntent("Native Plant Guide");
                break;

            case R.id.back_icon:
                finish();
                break;
        }

    }

    private void callIntent(String headerStr) {
        Intent intent = new Intent(this, AboutusActivity.class);
        intent.putExtra("header",headerStr);
        this.startActivity(intent);
    }
}
