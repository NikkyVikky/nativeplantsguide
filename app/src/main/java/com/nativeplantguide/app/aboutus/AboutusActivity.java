package com.nativeplantguide.app.aboutus;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nativeplantguide.app.R;
import com.nativeplantguide.app.aboutus.Adapter.AboutTabsAdapter;
import com.nativeplantguide.app.customViews.NativePlantTextView;

/**
 * This Activity class is used to display the about us screen and its parent activity of about us related fragments
 *
 * @author Innoppl Technologies.
 * @version 1.0
 */
public class AboutusActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar;
    private String headerStr;

    /**
     * To intialize the parent layout of about us screen
     * @param savedInstanceState its used to store the screen's data
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        tabLayout = (TabLayout)findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.arabic)));
        tabLayout.addTab(tabLayout.newTab().setText("English"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        viewPager =(ViewPager)findViewById(R.id.view_pager);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ((ImageView)toolbar.findViewById(R.id.back_icon)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();

            }
        });
        NativePlantTextView toolbar_title=findViewById(R.id.toolbar_title);
        if(getIntent()!=null){

            headerStr=getIntent().getStringExtra("header");
            toolbar_title.setText(headerStr);
        }

        AboutTabsAdapter tabsAdapter = new AboutTabsAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), headerStr );
        viewPager.setAdapter(tabsAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            //noinspection ConstantConditions
            TextView tv = (TextView) LayoutInflater.from(AboutusActivity.this).inflate(R.layout.textview_tab,null);
            tabLayout.getTabAt(i).setCustomView(tv);
        }
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    /**
     * The override method will be called when the activity will be killed
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
