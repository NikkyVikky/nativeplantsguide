package com.nativeplantguide.app.aboutus;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nativeplantguide.app.R;
import com.nativeplantguide.app.customViews.NativePlantTextView;
import com.nativeplantguide.app.customViews.NativePlantTextViewLight;

/**
 * This Fragment class is used to display the about us screen of Al Ain Municipality with arabic content
 *
 * @author Innoppl Technologies.
 * @version 1.0
 */
public class AboutArFragment extends Fragment {

    private NativePlantTextViewLight aboutContent1, aboutContent2, aboutvision, aboutmission;
    private NativePlantTextView vision, mission;

    /**
     * The override method used to inflate about us view in parent activity
     * @param inflater used to initialize the xml layout
     * @param container root of the view
     * @param savedInstanceState its used to store the screen's data
     * @return To display the about us view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_about_ar, container, false);
        aboutContent1 = (NativePlantTextViewLight)rootView.findViewById(R.id.about_cnt);
        aboutContent2 = (NativePlantTextViewLight)rootView.findViewById(R.id.about_cnt1);
        aboutmission = (NativePlantTextViewLight)rootView.findViewById(R.id.mission_ctn);
        aboutvision = (NativePlantTextViewLight)rootView.findViewById(R.id.vision_ctn);
        vision = (NativePlantTextView) rootView.findViewById(R.id.vision);
        mission = (NativePlantTextView) rootView.findViewById(R.id.mision);

        aboutContent1.setText(getActivity().getResources().getString(R.string.about_ar1));
        aboutContent2.setText(getActivity().getResources().getString(R.string.about_ar2));
        aboutmission.setText(getActivity().getResources().getString(R.string.about_ar_mission_ct));
        aboutvision.setText(getActivity().getResources().getString(R.string.about_ar_vision_ct));
        vision.setText(getActivity().getResources().getString(R.string.about_ar_vision));
        mission.setText(getActivity().getResources().getString(R.string.about_ar_mission));
        return rootView;
    }


}
