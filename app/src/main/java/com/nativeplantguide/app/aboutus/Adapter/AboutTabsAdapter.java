package com.nativeplantguide.app.aboutus.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.nativeplantguide.app.aboutus.AboutArFragment;
import com.nativeplantguide.app.aboutus.AboutEnFragment;
import com.nativeplantguide.app.aboutus.AboutNativeArfragment;
import com.nativeplantguide.app.aboutus.AboutNativeEnFragment;
/**
 * This adapter class is used to display the about us info both english and arabic
 *
 * @author Innoppl Technologies.
 * @version 1.0
 */
public class AboutTabsAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;
    private String headerStr;

    /**
     * constructor for AboutTabsAdapter
     * @param fm The fragments to add
     * @param NoofTabs Number of pages
     * @param headerStr Set the relevant header of the content
     */
    public AboutTabsAdapter(FragmentManager fm, int NoofTabs,String headerStr) {
        super(fm);
        this.mNumOfTabs = NoofTabs;
        this.headerStr=headerStr;

    }

    /**
     * The override method used to get a current item
     * @param i Position of the page
     * @return The selected view should be return
     */
    @Override
    public Fragment getItem(int i) {
        Fragment fragment;
        switch (i){
            case 0:

                if (headerStr.contentEquals("Al Amin Municipality")) {
                    fragment = new AboutArFragment();
                }else {
                    fragment =new AboutNativeArfragment();
                }
                return fragment;
            case 1:

                if (headerStr.contentEquals("Al Amin Municipality")) {
                    fragment = new AboutEnFragment();
                }else {
                    fragment =new AboutNativeEnFragment();
                }
                return fragment;
            default:
                return null;
        }
    }

    /**
     * The override method used to get size of view list
     * @return Returns to total count of pages
     */
    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
