package com.nativeplantguide.app.aboutus;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nativeplantguide.app.R;
import com.nativeplantguide.app.customViews.NativePlantTextView;
import com.nativeplantguide.app.customViews.NativePlantTextViewLight;
/**
 * This fragment used to display the about us screen of Al Ain Municipality with english content
 *
 * @author Innoppl Technologies.
 * @version 1.0
 */
public class AboutEnFragment extends Fragment {
    NativePlantTextViewLight aboutContent1, aboutvision, aboutmission;
    NativePlantTextView vision, mission;

    /**
     * The override method used to inflate about us view in parent activity
     * @param inflater used to initialize the xml layout
     * @param container root of the view
     * @param savedInstanceState its used to store the screen's data
     * @return To display the about us view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_en_about, container, false);
        aboutContent1 = (NativePlantTextViewLight)rootView.findViewById(R.id.about_cnt);
        aboutmission = (NativePlantTextViewLight)rootView.findViewById(R.id.mission_ctn);
        aboutvision = (NativePlantTextViewLight)rootView.findViewById(R.id.vision_ctn);
        vision = (NativePlantTextView) rootView.findViewById(R.id.vision);
        mission = (NativePlantTextView) rootView.findViewById(R.id.mision);

        aboutContent1.setText(getActivity().getResources().getString(R.string.about_en));
        aboutmission.setText(getActivity().getResources().getString(R.string.about_mission_ct_en));
        aboutvision.setText(getActivity().getResources().getString(R.string.about_vision_ct_en));
        vision.setText(getActivity().getResources().getString(R.string.about_vision_en));
        mission.setText(getActivity().getResources().getString(R.string.about_mission_en));
        return rootView;
    }
}
