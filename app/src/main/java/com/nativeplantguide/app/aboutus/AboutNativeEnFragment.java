package com.nativeplantguide.app.aboutus;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nativeplantguide.app.R;
import com.nativeplantguide.app.customViews.NativePlantTextView;
import com.nativeplantguide.app.customViews.NativePlantTextViewLight;
/**
 * This Fragment class is used to display the about us screen of Native plants with english content
 *
 * @author Innoppl Technologies.
 * @version 1.0
 */
public class AboutNativeEnFragment extends Fragment {
    private NativePlantTextViewLight para1, bul1;
    private NativePlantTextView title;

    /**
     * The override method used to inflate about us view in parent activity
     * @param inflater used to initialize the xml layout
     * @param container root of the view
     * @param savedInstanceState its used to store the screen's data
     * @return To display the about us view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_native_en, container, false);
        title = (NativePlantTextView)rootView.findViewById(R.id.title);
        para1 = (NativePlantTextViewLight)rootView.findViewById(R.id.para1);
        bul1 = (NativePlantTextViewLight)rootView.findViewById(R.id.bul1);
        String circle = "\u25CF";
        String sentence = circle + "  " +getActivity().getResources().getString(R.string.about_native_bul1_en)+ "\n\n"+
                circle + "  " +getActivity().getResources().getString(R.string.about_native_bul2_en)+"\n\n"+
                circle + "  " +getActivity().getResources().getString(R.string.about_native_bul3_en);
        title.setText(getActivity().getResources().getString(R.string.about_native_en_title));
        para1.setText(getActivity().getResources().getString(R.string.about_native_para1_en));
        bul1.setText(sentence);
        return rootView;
    }
}
