package com.nativeplantguide.app.aboutus;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nativeplantguide.app.R;
import com.nativeplantguide.app.customViews.NativePlantTextViewLight;
/**
 * This Fragment class is used to display the about us screen of Native plants with arabic content
 *
 * @author Innoppl Technologies.
 * @version 1.0
 */
public class AboutNativeArfragment extends Fragment {

    /**
     * The override method used to inflate about us view in parent activity
     * @param inflater used to initialize the xml layout
     * @param container root of the view
     * @param savedInstanceState its used to store the screen's data
     * @return To display the about us view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_native_ar, container, false);
        NativePlantTextViewLight aboutNativeArTv=rootView.findViewById(R.id.aboutNativeArTv);
        aboutNativeArTv.setText(getResources().getString(R.string.about_native_ar));
        return rootView;
    }

}
