package com.nativeplantguide.app.customViews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;
/**
 * This class is customized the Textview
 * Added our custom font for ubuntu light
 */
@SuppressLint("AppCompatCustomView")
public class NativePlantTextViewLight extends TextView{
    public NativePlantTextViewLight(Context context) {
        super(context);
        init();
    }

    public NativePlantTextViewLight(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NativePlantTextViewLight(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public NativePlantTextViewLight(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Ubuntu-L.ttf");
        setTypeface(tf ,1);

    }
}
