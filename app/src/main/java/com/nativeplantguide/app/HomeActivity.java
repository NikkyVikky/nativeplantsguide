package com.nativeplantguide.app;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nativeplantguide.app.aboutus.AboutUsMenuActivity;
import com.nativeplantguide.app.aboutus.AboutusActivity;
import com.nativeplantguide.app.camera2.CameraActivity;
import com.nativeplantguide.app.plantdetails.PlantDetailsActivity;
import com.nativeplantguide.app.plantdetails.PlantFullDetailArActivity;
import com.nativeplantguide.app.plantdetails.PlantFullDetailEngActivity;

/**
 * This Activity is used to display home screen
 *
 * @author Innoppl Technologies
 * @version 1.0
 */
public class HomeActivity extends AppCompatActivity {
    private RelativeLayout libraryButton, scaneButton;
    private EditText searchBox;
    private Toolbar toolbar;
    private RelativeLayout searchLayout, parentLayout;
    private ImageView searchBtn;
    private AutoCompleteTextView autoCompleteTextView;
    private boolean isEnglish;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        scaneButton = (RelativeLayout)findViewById(R.id.scan_button);
        libraryButton = (RelativeLayout)findViewById(R.id.library_button);
        searchLayout = (RelativeLayout)findViewById(R.id.search_layout);
        searchBtn = (ImageView)findViewById(R.id.search_btn);
        autoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.search_view);
        parentLayout = (RelativeLayout)findViewById(R.id.parent_layout);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        autoCompleteTextView = findViewById(R.id.search_view);
        setSupportActionBar(toolbar);
        ((ImageView)toolbar.findViewById(R.id.search_icon)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchLayout.setVisibility(View.VISIBLE);
                ((ImageView)toolbar.findViewById(R.id.search_icon)).setVisibility(View.INVISIBLE);
                // Request focus and show soft keyboard automatically
               showSoftKeyboard(HomeActivity.this);
                Log.e("Home","Language>>"+getKeyboardLanguage());
                String language = "en_US";
                if(getKeyboardLanguage().toLowerCase().equals(language.toLowerCase())){
                    isEnglish = true;
                    adapter = new ArrayAdapter<String>
                            (HomeActivity.this, android.R.layout.select_dialog_item, getResources().getStringArray(R.array.name_en));
                    autoCompleteTextView.setAdapter(adapter);
                }else{
                    isEnglish = false;
                    adapter = new ArrayAdapter<String>
                            (HomeActivity.this, android.R.layout.select_dialog_item, getResources().getStringArray(R.array.name_ar));
                    autoCompleteTextView.setAdapter(adapter);
                }

            }
        });
        toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.info_icon));
        ((ImageView)toolbar.findViewById(R.id.info_icon)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, AboutUsMenuActivity.class));

            }
        });
        scaneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, CameraActivity.class);
                startActivity(intent);
            }
        });
        libraryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, PlantDetailsActivity.class);
                intent.putExtra("library","library");
                startActivity(intent);
            }
        });
        parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard(HomeActivity.this);
                if(searchLayout.getVisibility() == View.VISIBLE){
                    searchLayout.setVisibility(View.GONE);
                }
                ((ImageView)toolbar.findViewById(R.id.search_icon)).setVisibility(View.VISIBLE);
            }
        });

        /**
         * This listener used to get auto suggest from search text
         */
        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.e("Home","Language>>1"+getKeyboardLanguage());
                String language = "en_US";
                if(getKeyboardLanguage().toLowerCase().equals(language.toLowerCase())){
                    isEnglish = true;
                }else{
                    isEnglish = false;
                }
            }
        });
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String selectionStr = (String)parent.getItemAtPosition(position);
                Intent intent;
                String[] plantTitle;
                if(isEnglish) {
                    plantTitle = getResources().getStringArray(R.array.name_en);
                    intent = new Intent(HomeActivity.this, PlantFullDetailEngActivity.class);
                }else {
                    plantTitle = getResources().getStringArray(R.array.name_ar);
                    intent = new Intent(HomeActivity.this, PlantFullDetailArActivity.class);
                }
                Log.d("Search","Search "+selectionStr);
                Log.e("Search","Search> "+parent.getPositionForView(view));
                for(int i=0; i<plantTitle.length;i++){
                    if(selectionStr.equals(plantTitle[i])){
                        position = i;
                        break;
                    }
                }

                intent.putExtra("position",position);
                startActivity(intent);
                autoCompleteTextView.setText("");
                searchLayout.setVisibility(View.GONE);
                ((ImageView)toolbar.findViewById(R.id.search_icon)).setVisibility(View.VISIBLE);
            }
        });
        getPermissions();
        if(isEnglish) {
            this.adapter = new ArrayAdapter<String>
                    (HomeActivity.this, android.R.layout.select_dialog_item, getResources().getStringArray(R.array.name_en));
            autoCompleteTextView.setAdapter(this.adapter);
        }else{
            this.adapter = new ArrayAdapter<String>
                    (HomeActivity.this, android.R.layout.select_dialog_item, getResources().getStringArray(R.array.name_ar));
            autoCompleteTextView.setAdapter(this.adapter);
        }
        this.adapter.setNotifyOnChange(true);
    }

    /**
     * This method used to get run permission for respective events
     */
    private void getPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                //Requesting permission.
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }
    }

    @Override //Override from ActivityCompat.OnRequestPermissionsResultCallback Interface
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted
                }
                return;
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void showSoftKeyboard(Activity activity){
        InputMethodManager inputMethodManager =
                (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(),
                InputMethodManager.SHOW_FORCED, 0);
    }

    public String getKeyboardLanguage(){
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        InputMethodSubtype ims = imm.getCurrentInputMethodSubtype();
        String locale = ims.getLocale();
        return locale;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.about, menu);
        for(int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            SpannableString s = new SpannableString(item.getTitle());
            s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.buttonColors)), 0, s.length(), 0);
            item.setTitle(s);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.al_ain:
                callIntent("Al Ain City Municipality");
                break;
            case R.id.native_palnt:
                callIntent("Native Plant Guide");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /** This method used to call about us activity
     *
     * @param headerStr title of the activity
     */
    private void callIntent(String headerStr) {
        Intent intent = new Intent(this, AboutusActivity.class);
        intent.putExtra("header",headerStr);
        this.startActivity(intent);
    }
}
