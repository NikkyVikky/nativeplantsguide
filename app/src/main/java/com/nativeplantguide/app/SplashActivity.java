package com.nativeplantguide.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * This Activity used to display splash scree
 *
 * @author Innoppl Technologies
 * @version 1.0
 */
public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                passIntent();
            }
        }, 5000);
    }

    /**
     * This method used to call home screen
     */
    void passIntent() {
        Intent intent;
        intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
