package com.nativeplantguide.app.libarylist;

import android.view.View;

/**
 * This interface is listener of library list item
 *
 * @author Innoppl Technologies
 * @version 1.0
 */
public interface ItemClickListener {
    void onItemClicked(View view, int position);
}
