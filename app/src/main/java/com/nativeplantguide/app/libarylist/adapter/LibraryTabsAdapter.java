package com.nativeplantguide.app.libarylist.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.nativeplantguide.app.libarylist.LibraryListArFragment;
import com.nativeplantguide.app.libarylist.LibraryListEngFragment;

/**
 * This adapter class to add the library list of both arabic and english
 *
 * @author Innoppl Technologies
 * @version 1.0
 */
public class LibraryTabsAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;
    public LibraryTabsAdapter(FragmentManager fm, int NoofTabs) {
        super(fm);
        this.mNumOfTabs = NoofTabs;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0:
                LibraryListArFragment arabic = new LibraryListArFragment();
                return arabic;
            case 1:
                LibraryListEngFragment english = new LibraryListEngFragment();
                return english;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
