package com.nativeplantguide.app.libarylist.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.nativeplantguide.app.R;
import com.nativeplantguide.app.libarylist.ItemClickListener;
import com.nativeplantguide.app.libarylist.pojo.LibraryListPojo;

import java.util.ArrayList;
import java.util.List;

/**
 * This adapter used to display library list
 *
 * @author Innoppl Technologies
 * @version 1.0
 */
public class LibraryListRecyclerAdapter extends RecyclerView.Adapter<LibraryListViewHolder> implements Filterable {
    public List<LibraryListPojo> libraryListPojo;
    private final ItemClickListener itemClickListener;
    private List<LibraryListPojo> libraryListFilterArr;
    public LibraryListRecyclerAdapter(List<LibraryListPojo> libraryListPojo, ItemClickListener itemClickListener){
        this.libraryListPojo = libraryListPojo;
        this.itemClickListener = itemClickListener;
        this.libraryListFilterArr = libraryListPojo;
    }

    @Override
    public LibraryListViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.library_list_item, viewGroup, false);

        return new LibraryListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LibraryListViewHolder holder, final int i) {
        LibraryListPojo libraryPojo = libraryListPojo.get(i);
        //int[] intArr = new int[]{R.drawable.covolu, R.drawable.senna_italica, R.drawable.ochradenus_arabicus, R.drawable.pulicari_glutinosa, R.drawable.tephrosia_apollinea, R.drawable.aerva_javanica};
        holder.plantName.setText(libraryPojo.getPlantName());
        holder.plantDesc.setText(libraryPojo.getPlantDesc());
        holder.plantImage.setImageResource(libraryPojo.getDrawable());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClicked(view, i);
            }
        });
    }



    @Override
    public int getItemCount() {
        return libraryListPojo.size();
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    libraryListFilterArr = libraryListPojo;
                } else {
                    List<LibraryListPojo> filteredList = new ArrayList<>();
                    for (LibraryListPojo row : libraryListPojo) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getPlantName().toLowerCase().contains(charString.toLowerCase()) || row.getPlantDesc().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    libraryListFilterArr = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = libraryListFilterArr;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                libraryListFilterArr = (ArrayList<LibraryListPojo>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    /**
     * This method used to update search result
     * @param list result of search text
     */
    public void updateList(List<LibraryListPojo> list){
        libraryListPojo = list;
        notifyDataSetChanged();
    }

}
