package com.nativeplantguide.app.libarylist.pojo;

/**
 * This model class is used to handle the library list's data
 *
 * @author Innoppl Technologies
 * @version 1.0
 */
public class LibraryListPojo {
    private String plantName;
    private String plantDesc;

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    private int drawable;

    public String getPlantName() {
        return plantName;
    }

    public void setPlantName(String plantName) {
        this.plantName = plantName;
    }

    public String getPlantDesc() {
        return plantDesc;
    }

    public void setPlantDesc(String plantDesc) {
        this.plantDesc = plantDesc;
    }
}
