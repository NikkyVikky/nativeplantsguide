package com.nativeplantguide.app.libarylist.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.nativeplantguide.app.R;
import com.nativeplantguide.app.customViews.NativePlantTextView;
import com.nativeplantguide.app.customViews.NativePlantTextViewLight;

/**
 * This view holder class used to configure the library list item
 *
 * @author Innoppl Technologies
 * @version  1.0
 */
public class LibraryListViewHolder extends RecyclerView.ViewHolder {
    public NativePlantTextView plantName;
    public NativePlantTextViewLight plantDesc;
    public ImageView plantImage;

    public LibraryListViewHolder(View itemView) {
        super(itemView);
        plantName =  (NativePlantTextView)itemView.findViewById(R.id.plant_name);
        plantDesc = (NativePlantTextViewLight)itemView.findViewById(R.id.plant_desc);
        plantImage = (ImageView)itemView.findViewById(R.id.plant_image);
    }
}
