package com.nativeplantguide.app.libarylist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.nativeplantguide.app.HomeActivity;
import com.nativeplantguide.app.R;
import com.nativeplantguide.app.libarylist.adapter.LibraryListRecyclerAdapter;
import com.nativeplantguide.app.libarylist.pojo.LibraryListPojo;
import com.nativeplantguide.app.plantdetails.PlantDetailsActivity;
import com.nativeplantguide.app.plantdetails.PlantFullDetailArActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * The Fragment is used to display library list screen with arabic
 *
 * @author Innoppl Technologies
 * @version 1.0
 */
public class LibraryListArFragment extends Fragment implements ItemClickListener {

    private RecyclerView libraryListView;
    private LibraryListRecyclerAdapter adapter;
    private List<LibraryListPojo> libraryListArr = new ArrayList<>();
    private LibraryListPojo libraryListPojo;
    private String[] plantName, plantDesc;
    private LinearLayout parent;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_plant_library_en, container, false);
        libraryListView = (RecyclerView)rootView.findViewById(R.id.library_list);
        plantName = getActivity().getResources().getStringArray(R.array.name_ar);
        plantDesc = getActivity().getResources().getStringArray(R.array.plant_desc_ar);
        parent = (LinearLayout)rootView.findViewById(R.id.parent);
        parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.hideSoftKeyboard(getActivity());
            }
        });
        int[] intArr = new int[]{R.drawable.covolu, R.drawable.senna_italica, R.drawable.ochradenus_arabicus, R.drawable.pulicari_glutinosa, R.drawable.tephrosia_apollinea, R.drawable.aerva_javanica};
        for(int i=0; i<plantName.length; i++){
            libraryListPojo = new LibraryListPojo();
            libraryListPojo.setPlantName(plantName[i]);
            libraryListPojo.setPlantDesc(plantDesc[i]);
            libraryListPojo.setDrawable(intArr[i]);
            libraryListArr.add(libraryListPojo);
        }
        adapter = new LibraryListRecyclerAdapter(libraryListArr, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        libraryListView.setLayoutManager(mLayoutManager);
        libraryListView.setItemAnimator(new DefaultItemAnimator());
        libraryListView.setAdapter(adapter);
        return rootView;
    }

    @Override
    public void onItemClicked(View view, int position) {
        Intent intent = new Intent(getActivity(), PlantFullDetailArActivity.class);
        intent.putExtra("position", position);
        getActivity().startActivity(intent);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((PlantDetailsActivity)getActivity()).autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String selectionStr = ((PlantDetailsActivity)getActivity()).autoCompleteTextView.getText().toString().trim();
                filter(selectionStr);
            }
        });
    }

    /**
     * This method used to filter based on the search text
     * @param text search text
     */
    void filter(String text){
        List<LibraryListPojo> temp = new ArrayList();

        for(LibraryListPojo d: libraryListArr){
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if(d.getPlantName().toLowerCase().contains(text.toLowerCase())){
                temp.add(d);
            }
        }
        //update recyclerview
        adapter.updateList(temp);
    }
}
