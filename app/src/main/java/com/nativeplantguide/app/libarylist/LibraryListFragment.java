package com.nativeplantguide.app.libarylist;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nativeplantguide.app.HomeActivity;
import com.nativeplantguide.app.R;
import com.nativeplantguide.app.libarylist.adapter.LibraryTabsAdapter;
import com.nativeplantguide.app.plantdetails.PlantDetailsActivity;

/**
 * The Fragment is used to display library list screen with english and arabic tabs
 *
 * @author Innoppl Technologies
 * @version 1.0
 */
public class LibraryListFragment extends Fragment{
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int position;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_plant_library, container, false);
        tabLayout = (TabLayout)rootView.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.arabic)));
        tabLayout.addTab(tabLayout.newTab().setText("English"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        viewPager =(ViewPager)rootView.findViewById(R.id.view_pager);
        LibraryTabsAdapter tabsAdapter = new LibraryTabsAdapter(getActivity().getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(tabsAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            //noinspection ConstantConditions
            TextView tv = (TextView)LayoutInflater.from(getActivity()).inflate(R.layout.textview_tab,null);
            tabLayout.getTabAt(i).setCustomView(tv);
        }
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        ((PlantDetailsActivity)getActivity()).searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.showSoftKeyboard(getActivity());
            }
        });
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


}
