package com.nativeplantguide.app.camera2;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.TextureView;
import android.view.View;
import android.widget.RelativeLayout;

import com.nativeplantguide.app.R;
import com.nativeplantguide.app.imagescanning.ImageScanningActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * This Activity class is used to display the camera view
 *
 * @author Innoppl Technologies.
 * @version 1.0
 */
public class CameraActivity extends AppCompatActivity implements CameraControllerV2.ImageCaptureButtonCallback{
    private TextureView textureView;
    private RelativeLayout takePicture;
    private CameraControllerV2 cameraControllerV2;
    private boolean isEnableCapture = true;
    private Toolbar toolbar;
    private RelativeLayout backButton;
    private RelativeLayout parent;
    private Uri mImageCaptureCroppedUri;
    private Bitmap bitmap;

    /**
     * To intialize the parent layout of camera screen
     * @param savedInstanceState its used to store the screen's data
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_preview);
        initializeViews();

        cameraControllerV2 = new CameraControllerV2(CameraActivity.this, textureView, this);
        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CameraActivity.this, ImageScanningActivity.class);
                bitmap = textureView.getBitmap();//getScreenShot(parent);
                File file = null;
                try {
                    file = store(bitmap,getImageFile());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                intent.putExtra("image_path", file.getAbsolutePath());
                startActivity(intent);
                finish();
            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    /**
     * To initialize the child views of parent layout
     */
    private void initializeViews(){
        textureView = (TextureView) findViewById(R.id.texture_view);
        takePicture = (RelativeLayout) findViewById(R.id.capture_button);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        parent = (RelativeLayout)findViewById(R.id.camara_layout);
        setSupportActionBar(toolbar);
        backButton = (RelativeLayout)toolbar.findViewById(R.id.back_btn);
    }

    /**
     * To handle the camera button multi click
     */
    @Override
    public void captureButtonEnable() {
        isEnableCapture = true;
    }

    /**
     * To handle the camera button multi click
     */
    @Override
    public void captureButtonDisable() {
        isEnableCapture = true;
    }

    /**
     * This method used to create a new image file
     * @param bm The bitmap is add to the file
     * @param saveFilePath File path of image source
     * @return
     */
    public File store(Bitmap bm, File saveFilePath) {
        File dir = new File(saveFilePath.getAbsolutePath());
        if (!dir.exists())
            dir.mkdirs();
        File file = saveFilePath;
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    /**
     * This method used to get the captured image
     * @return The image file
     * @throws IOException
     */
    public File getImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        return image;
    }

}
