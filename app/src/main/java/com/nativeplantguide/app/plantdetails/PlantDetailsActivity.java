package com.nativeplantguide.app.plantdetails;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nativeplantguide.app.R;
import com.nativeplantguide.app.customViews.NativePlantTextView;
import com.nativeplantguide.app.libarylist.LibraryListFragment;

/**
 * The Activity is used to display plant full description with arabic and english tabs
 *
 * @author Innoppl Technologies
 * @version 1.0
 */
public class PlantDetailsActivity extends AppCompatActivity {
    private FrameLayout frameLayout;
    private Toolbar toolbar;
    public int position = -1;
    public ImageView searchBtn;
    public AutoCompleteTextView autoCompleteTextView;
    public RelativeLayout searchLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant_details);
        initializeView();
        ((ImageView)toolbar.findViewById(R.id.back_icon)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if(getIntent().getExtras().containsKey("position")){
            position = getIntent().getExtras().getInt("position");
            Log.e("Length>>", ""+position);
        }else{
            Log.e("Position Not came>>", ""+position);
        }
        ((ImageView)toolbar.findViewById(R.id.library_icon)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] parts = getResources().getResourceName(R.id.library_icon).split("/");
                String id = parts[1];
                if(id.trim().equals("library_icon")){
                    ((ImageView)toolbar.findViewById(R.id.library_icon)).setImageResource(R.drawable.search_icon);
                    LibraryListFragment libraryListFragment = new LibraryListFragment();
                    pushFragment(libraryListFragment, libraryListFragment.getClass().getName());
                }
                Log.e("Check", id );
            }
        });
        ((ImageView)toolbar.findViewById(R.id.back_icon)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if(getIntent().getExtras().containsKey("library")){
            ((ImageView)toolbar.findViewById(R.id.library_icon)).setImageResource(R.drawable.search_icon);
            LibraryListFragment libraryListFragment = new LibraryListFragment();
            pushFragment(libraryListFragment, libraryListFragment.getClass().getName());
        }else if(getIntent().getExtras().containsKey("scanning")){
            ((ImageView)toolbar.findViewById(R.id.library_icon)).setImageResource(R.drawable.ic_n_library);
            Bundle bundle = new Bundle();
            bundle.putInt("position", position);
            PlantDetailTabsFragment plantTabFragment = new PlantDetailTabsFragment();
            plantTabFragment.setArguments(bundle);
            pushFragment(plantTabFragment, plantTabFragment.getClass().getName());
        }
    }

    /**
     * This method used to add the fragment in parent activity
     * @param fragment fragment name
     * @param tag
     */
    public void pushFragment(Fragment fragment, String tag) {

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.plant_frame, fragment)
                .addToBackStack(tag)
                .commit();
    }

    /**
     * This method used to pop the fragment from the stack
     * @param tag
     */
    public void popFragment(String tag){
        getSupportFragmentManager().popBackStack(tag, getSupportFragmentManager().POP_BACK_STACK_INCLUSIVE);
    }

    /**
     * This method used to configure the child views of parent layout
     */
    public void initializeView(){
        frameLayout = (FrameLayout)findViewById(R.id.plant_frame);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ((NativePlantTextView)toolbar.findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.app_title));
        searchLayout = (RelativeLayout)findViewById(R.id.search_layout);
        autoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.search_view);
        searchBtn = (ImageView)toolbar.findViewById(R.id.library_icon);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
