package com.nativeplantguide.app.plantdetails;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.nativeplantguide.app.R;
import com.nativeplantguide.app.customViews.NativePlantTextView;
import com.nativeplantguide.app.customViews.NativePlantTextViewLight;

/**
 * The Activity is used to display plant full details with arabic
 *
 * @author Innoppl Technologies
 * @version 1.0
 */
public class PlantFullDetailArActivity extends AppCompatActivity {
    private NativePlantTextView plantTitle;
    private NativePlantTextViewLight plantDescription, plantFlowering, plantLocation;
    private ImageView imagePreview;
    String[] titleArr, descriptionArr, floweringArr, locationArr;
    private int position;
    private Toolbar toolbar;
    private WebView webView;
    private String firstPlant = "http://34.195.209.233/3dViewer/index_conv.html";
    private String secondPlant = "http://34.195.209.233/3dViewer/index_senna.html";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant_full_detail_ar);
        plantDescription = (NativePlantTextViewLight)findViewById(R.id.description);
        plantFlowering = (NativePlantTextViewLight)findViewById(R.id.flowering_detail);
        plantLocation = (NativePlantTextViewLight)findViewById(R.id.location_detail);
        plantTitle = (NativePlantTextView)findViewById(R.id.title);
        imagePreview = findViewById(R.id.image_preview);
        webView = (WebView)findViewById(R.id.webview);
        position = getIntent().getExtras().getInt("position");

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ((NativePlantTextView)toolbar.findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.app_title));

        ((ImageView)toolbar.findViewById(R.id.back_icon)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        titleArr = getResources().getStringArray(R.array.name_ar);
        descriptionArr = getResources().getStringArray(R.array.plant_desc_ar);
        floweringArr = getResources().getStringArray(R.array.flowering_ar);
        locationArr = getResources().getStringArray(R.array.location_and_distribution_ar);

        ((NativePlantTextView)toolbar.findViewById(R.id.toolbar_title)).setText(titleArr[position]);

        plantTitle.setText(titleArr[position]);
        plantLocation.setText(locationArr[position]);
        plantFlowering.setText(floweringArr[position]);
        plantDescription.setText(descriptionArr[position]);
        if(position == 0){
            imagePreview.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
            load3DModel(firstPlant);
        }else if(position == 1){
            imagePreview.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
            load3DModel(secondPlant);
        }else {
            imagePreview.setVisibility(View.VISIBLE);
            webView.setVisibility(View.GONE);
            int[] intArr = new int[]{R.drawable.covolu, -1, R.drawable.ochradenus_arabicus, R.drawable.pulicari_glutinosa, R.drawable.tephrosia_apollinea, R.drawable.aerva_javanica};
            imagePreview.setImageResource(intArr[position]);
        }
    }

    /**
     * This method used to load the 3D model of the plant
     * @param url web url of plant model
     */
    public void load3DModel(String url){
        webView.getSettings().setDisplayZoomControls(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);//);
    }

    /**
     * This method used to clear the cache from webview
     */
    public void releaseUrl(){
        webView.clearHistory();

        // NOTE: clears RAM cache, if you pass true, it will also clear the disk cache.
        // Probably not a great idea to pass true if you have other WebViews still alive.
        webView.clearCache(true);

        // Loading a blank page is optional, but will ensure that the WebView isn't doing anything when you destroy it.
        webView.loadUrl("about:blank");

        webView.onPause();
        webView.removeAllViews();
        webView.destroyDrawingCache();

        // NOTE: This pauses JavaScript execution for ALL WebViews,
        // do not use if you have other WebViews still alive.
        // If you create another WebView after calling this,
        // make sure to call mWebView.resumeTimers().
        webView.pauseTimers();

        // NOTE: This can occasionally cause a segfault below API 17 (4.2)
        webView.destroy();

        // Null out the reference so that you don't end up re-using it.
        webView = null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseUrl();
    }
}
