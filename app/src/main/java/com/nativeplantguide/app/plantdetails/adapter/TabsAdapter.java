package com.nativeplantguide.app.plantdetails.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.nativeplantguide.app.plantdetails.ArabicTabFragment;
import com.nativeplantguide.app.plantdetails.EnglishTabFragment;

/**
 * This adapter class is used to display the Plant details info both english and arabic
 *
 * @author Innoppl Technologies.
 * @version 1.0
 */
public class TabsAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;
    private int position;
    private Bundle bundle;
    public TabsAdapter(FragmentManager fm, int NoofTabs, int plantPosition) {
        super(fm);
        this.mNumOfTabs = NoofTabs;
        position = plantPosition;
        bundle = new Bundle();
        bundle.putInt("position", position);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0:
                ArabicTabFragment arabic = new ArabicTabFragment();
                arabic.setArguments(bundle);
                return arabic;
            case 1:
                EnglishTabFragment english = new EnglishTabFragment();
                english.setArguments(bundle);
                return english;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
