package com.nativeplantguide.app.plantdetails;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nativeplantguide.app.R;
import com.nativeplantguide.app.customViews.NativePlantTextView;
import com.nativeplantguide.app.customViews.NativePlantTextViewLight;

/**
 * The Fragment is used to display plant details with english
 *
 * @author Innoppl Technologies
 * @version 1.0
 */
public class EnglishTabFragment extends Fragment {

    private NativePlantTextView title, readMore;;
    private NativePlantTextViewLight description;
    private String[] plantTitle;
    private String[] plantDescription;
    int position;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_plant_detail_eng, container, false);
        position = getArguments().getInt("position");
        title = (NativePlantTextView)rootView.findViewById(R.id.title);
        description = (NativePlantTextViewLight)rootView.findViewById(R.id.description);
        plantTitle = getActivity().getResources().getStringArray(R.array.name_en);
        plantDescription = getActivity().getResources().getStringArray(R.array.plant_desc_en);
        readMore =  (NativePlantTextView)rootView.findViewById(R.id.read_more);
        title.setText(plantTitle[position]);
        String desc = plantDescription[position];
        description.setText(desc);
        readMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PlantFullDetailEngActivity.class);
                intent.putExtra("position", position);
                getActivity().startActivity(intent);
            }
        });
        return rootView;
    }

}
