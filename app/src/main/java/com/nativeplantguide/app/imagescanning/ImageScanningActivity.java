package com.nativeplantguide.app.imagescanning;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.nativeplantguide.app.R;
import com.nativeplantguide.app.camera2.CameraActivity;
import com.nativeplantguide.app.plantdetails.PlantDetailsActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * This Activity used to display the Imagescanning screen
 * Implemented Firebase text recognition API
 * @author Innoppl Technologies
 * @version 1.0
 */
public class ImageScanningActivity extends AppCompatActivity {
    private ImageView imagePreview;
    private RelativeLayout closeButton;
    private FirebaseVisionImage image;
    private Integer mImageMaxWidth;
    // Max height (portrait mode)
    private Integer mImageMaxHeight;
    private FirebaseVisionTextRecognizer textRecognizer;

    /**
     * To intialize the parent layout of image scanning screen
     * @param savedInstanceState its used to store the screen's data
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanning);
        imagePreview = (ImageView)findViewById(R.id.image_preview);
        closeButton = (RelativeLayout)findViewById(R.id.close_button);
        textRecognizer = FirebaseVision.getInstance()
                .getOnDeviceTextRecognizer();
        if(getIntent().getExtras() != null) {
            if(getIntent().getExtras().containsKey("image_path")) {
                getBitmapfromLocal(getIntent().getExtras().getString("image_path"));
            }
        }

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ImageScanningActivity.this, CameraActivity.class));
                finish();
            }
        });
    }

    /**
     * This method used to handle back button press
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /**
     * This method used to convert the bitmap from the image file
     * @param filePath the path of the image file
     */
    private void getBitmapfromLocal(String filePath){
        File file = new File(filePath);
        if (file.exists()){
            Log.e("Camera", "File Exist");
        }else{
            Log.e("Camera", "File Not Exist");
        }

        try {
            File f = new File(filePath);
            ExifInterface exif = new ExifInterface(f.getPath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }

            Matrix mat = new Matrix();
            mat.postRotate(angle);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;

            Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f),
                    null, options);
            Bitmap bitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
                    bmp.getHeight(), mat, true);
            ByteArrayOutputStream outstudentstreamOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100,
                    outstudentstreamOutputStream);
            image = FirebaseVisionImage.fromBitmap(bitmap);
            imagePreview.setImageBitmap(bitmap);
            runTextRecognition();
            //imageView.setImageBitmap(bitmap);

        } catch (IOException e) {
            Log.w("TAG", "-- Error in setting image");
        } catch (OutOfMemoryError oom) {
            Log.w("TAG", "-- OOM Error in setting image");
        }
    }

    /**
     * This method used to recognition from image
     */
    private void runTextRecognition() {
        // TODO: Add your code here to run on-device text recognition.
        textRecognizer.processImage(image)
                .addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                    @Override
                    public void onSuccess(FirebaseVisionText result) {
                        String textArr[] = result.getText().split("\\r?\\n");
                        Log.e("Lenght",""+textArr.length);
                        Intent intent = new Intent(ImageScanningActivity.this, PlantDetailsActivity.class);
                        int position = -1;
                        for(int i=0; i<textArr.length; i++){
                            Log.e("Scanning",textArr[i]);
                            if(textArr[i].toLowerCase().indexOf("gov") !=-1){
                                position = 0;
                                break;
                            }else if(textArr[i].toLowerCase().indexOf("dhabi") !=-1){
                                position = 1;
                                break;
                            }else if(textArr[i].toLowerCase().indexOf("dhab") !=-1){
                                position = 1;
                                break;
                            }else if(textArr[i].toLowerCase().indexOf("dha") !=-1){
                                position = 1;
                                break;
                            }else if(textArr[i].toLowerCase().indexOf("habi") !=-1){
                                position = 1;
                                break;
                            }else if(textArr[i].toLowerCase().indexOf("hab") !=-1){
                                position = 1;
                                break;
                            } else{
                                position = -1;
                            }
                        }
                        // Task completed successfully
                        //Toast.makeText(ImageScanningActivity.this, result.getText(), Toast.LENGTH_LONG).show();
                        if(position != -1) {
                            intent.putExtra("position", position);
                            intent.putExtra("scanning", "scanning");
                            startActivity(intent);
                            finish();
                        }else{
                            //Toast.makeText(ImageScanningActivity.this, "The dedective plant not found", Toast.LENGTH_SHORT).show();
                            showDialog();
                            //finish();
                        }
                    }
                })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                // Task failed with an exception
                                e.printStackTrace();
                            }
                        });
    }

    /**
     * This method used to display the dialog of native plant deduction validation
     */
    public void showDialog(){
        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(ImageScanningActivity.this);

        builder.setMessage(getResources().getString(R.string.alert_message)+"\n\n"+getResources().getString(R.string.alert_message_ar))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        startActivity(new Intent(ImageScanningActivity.this, CameraActivity.class));
                        finish();
                    }
                }).setCancelable(false);
               /* .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })*/
                //.setIcon(android.R.drawable.ic_dialog_alert)
        AlertDialog dialog = builder.show();
        TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER);
        dialog.show();
    }
}
